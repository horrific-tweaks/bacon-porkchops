This resource pack changes the [Porkchop](https://minecraft.fandom.com/wiki/Cooked_Porkchop) food items into "Raw Bacon" and "Cooked Bacon" with the associated textures.

![Raw Bacon](./assets/minecraft/textures/item/porkchop.png)
![Cooked Bacon](./assets/minecraft/textures/item/cooked_porkchop.png)

This is to reflect the phrase often spoken by [Kiwi1Kenobi](https://www.twitch.tv/kiwi1kenobi), "bacon."
